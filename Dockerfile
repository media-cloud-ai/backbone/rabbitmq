ARG TAG
FROM rabbitmq:${TAG}

RUN rabbitmq-plugins enable rabbitmq_prometheus
